﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading {
    public class Program5 {

        public void Thread1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(Thread.CurrentThread.Name +"="+ i);

                int iHour = 0;
                int iMin = 0;
                int iSec = 1;

                Thread.Sleep(1);
                //Thread.Sleep(new TimeSpan(iHour, iMin, iSec));
            }
        }
    }

    public class Class5
    {
        public static void Execute()
        {
            Console.WriteLine("Before start thread");

            Program5 thr1 = new Program5();
            Program5 thr2 = new Program5();

            Thread tid1 = new Thread(new ThreadStart(thr1.Thread1));
            Thread tid2 = new Thread(new ThreadStart(thr2.Thread1));

            tid1.Name = "Thread 1";
            tid2.Name = "Thread 2";

            try
            {
                tid1.Start();
                tid2.Start();
            }
            catch (ThreadStateException tse)
            {
                Console.WriteLine(tse.ToString());
            }

            /*try
            {
                tid1.Abort();
                tid2.Abort();
            }
            catch (ThreadAbortException tae)
            {
                Console.WriteLine(tae.ToString());
            }*/

            tid1.Join();
            tid2.Join(new TimeSpan(0,0,1));

            Console.WriteLine("End of Main");
        }
    }
}
