﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading {
    public class Program1 {

        public static void Thread1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread1 {0}",i);
            }
        }

        public static void Thread2()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread2 {0}", i);
            }
        }
    }


    public class Class1
    {
        public static void Execute()
        {
            Console.WriteLine("Before start thread");
            
            Thread tid1 = new Thread(new ThreadStart(Program1.Thread1));
            Thread tid2 = new Thread(new ThreadStart(Program1.Thread2));

            tid1.Start();
            tid2.Start();
        }
    }

}

