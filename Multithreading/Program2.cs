﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading {
    class Program2 {
        public void Thread1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread 1 : {0}",i);
                
            }
        }

        public void Thread2()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread 2 : {0}",i);
            }
        }
    }

    public class Class2
    {
        public static void Execute()
        {
            Console.WriteLine("Before start thread");

            Program2 thr = new Program2();

            Thread tid1 = new Thread(new ThreadStart(thr.Thread1));
            Thread tid2 = new Thread(new ThreadStart(thr.Thread2));

            tid1.Start();
            tid2.Start();
        }
    }
}
