﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading {
    public class Program14 {
        public void Thread1()
        {
            Console.WriteLine(Thread.GetDomain());

            for (int i = 0; i < 10; i++)
            {
                Thread thr = Thread.CurrentThread;
                Console.WriteLine(thr.Name + i);
                Thread.Sleep(1);
            }
        }
    }

    public class Class14
    {
        public static void Execute()
        {
            Console.WriteLine("Before thread execution");

            Program14 trd1 = new Program14();
            Program14 trd2 = new Program14();
            
            Thread tid1= new Thread(new ThreadStart(trd1.Thread1));
            Thread tid2 = new Thread(new ThreadStart(trd2.Thread1));

            tid1.IsBackground = true;
            tid2.IsBackground = true;

            try
            {
                tid1.Start();
                tid2.Start();
            }
            catch (ThreadStateException te)
            {
                Console.WriteLine(te.ToString());
            }

            Thread.Sleep(10);
            Console.WriteLine("End of execute");

        }
    }
}
