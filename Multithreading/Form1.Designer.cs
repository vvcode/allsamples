﻿namespace Multithreading {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnProgram1 = new System.Windows.Forms.Button();
            this.btnProgram2 = new System.Windows.Forms.Button();
            this.btnProgram5 = new System.Windows.Forms.Button();
            this.btnProgram6 = new System.Windows.Forms.Button();
            this.btnJoin = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnProgram1
            // 
            this.btnProgram1.Location = new System.Drawing.Point(30, 29);
            this.btnProgram1.Name = "btnProgram1";
            this.btnProgram1.Size = new System.Drawing.Size(70, 39);
            this.btnProgram1.TabIndex = 0;
            this.btnProgram1.Text = "Program1";
            this.btnProgram1.UseVisualStyleBackColor = true;
            this.btnProgram1.Click += new System.EventHandler(this.btnProgram1_Click);
            // 
            // btnProgram2
            // 
            this.btnProgram2.Location = new System.Drawing.Point(139, 29);
            this.btnProgram2.Name = "btnProgram2";
            this.btnProgram2.Size = new System.Drawing.Size(70, 39);
            this.btnProgram2.TabIndex = 1;
            this.btnProgram2.Text = "Program2";
            this.btnProgram2.UseVisualStyleBackColor = true;
            this.btnProgram2.Click += new System.EventHandler(this.btnProgram2_Click);
            // 
            // btnProgram5
            // 
            this.btnProgram5.Location = new System.Drawing.Point(243, 29);
            this.btnProgram5.Name = "btnProgram5";
            this.btnProgram5.Size = new System.Drawing.Size(70, 39);
            this.btnProgram5.TabIndex = 2;
            this.btnProgram5.Text = "Program5";
            this.btnProgram5.UseVisualStyleBackColor = true;
            this.btnProgram5.Click += new System.EventHandler(this.btnProgram5_Click);
            // 
            // btnProgram6
            // 
            this.btnProgram6.Location = new System.Drawing.Point(30, 108);
            this.btnProgram6.Name = "btnProgram6";
            this.btnProgram6.Size = new System.Drawing.Size(70, 39);
            this.btnProgram6.TabIndex = 3;
            this.btnProgram6.Text = "Program14";
            this.btnProgram6.UseVisualStyleBackColor = true;
            this.btnProgram6.Click += new System.EventHandler(this.btnProgram6_Click);
            // 
            // btnJoin
            // 
            this.btnJoin.Location = new System.Drawing.Point(139, 108);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(70, 39);
            this.btnJoin.TabIndex = 4;
            this.btnJoin.Text = "Join";
            this.btnJoin.UseVisualStyleBackColor = true;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(243, 108);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(70, 39);
            this.button4.TabIndex = 5;
            this.button4.Text = "Program2";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 242);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnJoin);
            this.Controls.Add(this.btnProgram6);
            this.Controls.Add(this.btnProgram5);
            this.Controls.Add(this.btnProgram2);
            this.Controls.Add(this.btnProgram1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnProgram1;
        private System.Windows.Forms.Button btnProgram2;
        private System.Windows.Forms.Button btnProgram5;
        private System.Windows.Forms.Button btnProgram6;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.Button button4;
    }
}

