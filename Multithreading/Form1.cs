﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btnProgram1_Click(object sender, EventArgs e) {
            Class1.Execute();
        }

        private void btnProgram2_Click(object sender, EventArgs e) {
            Class2.Execute();
        }

        private void btnProgram5_Click(object sender, EventArgs e)
        {
            Class5.Execute();
        }

        private void btnProgram6_Click(object sender, EventArgs e) {
            Class14.Execute();
        }

        private void btnJoin_Click(object sender, EventArgs e) {
            Class15.Execute();
        }
    }
}
